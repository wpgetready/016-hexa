﻿using UnityEngine;
using UnityEngine.UI;

namespace MadFireOn
{

    public class TimerChall : MonoBehaviour
    {

        public Image fillImg;               //ref to fill image
        public Color firstColor, secondColor, thirdColor;     //color of image
        public float timeAmt;

        [HideInInspector]
        public float time;                  //time which we track

        void Start()
        {
            time = timeAmt;
        }

        void Update()
        {
            if (GameManager.instance.gameOver || LevelGuiManager.instance.levelComplete || LevelGuiManager.instance.gamePause) return; //we return

            ChangeColor();                                  //change the color of fill image

            if (time >= 0)                                  //time is more than zero
            {
                time -= Time.deltaTime;                     //we reduce time by deltaTime
                fillImg.fillAmount = time / timeAmt;        //we set fillAmount of fillImage

                if (time <= 0)
                {
                    time = 0;
                    fillImg.fillAmount = 0;

                    GameManager.instance.gameOver = true;   //End Time
                }
            }
        }

        private void ChangeColor()
        {
            if (fillImg.fillAmount > 0.75f) fillImg.color = firstColor;        //Between 1.00 and 0.75
            else if (fillImg.fillAmount > 0.5f && fillImg.fillAmount < 0.75f) fillImg.color = secondColor;       //Between 0.75 and 0.50
            else if (fillImg.fillAmount < 0.5f) fillImg.color = thirdColor;        //Between 0.50 and 0.00
        }
    }
}