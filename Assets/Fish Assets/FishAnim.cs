﻿using System.Collections;
using UnityEngine;
using MadFireOn;


public class FishAnim : MonoBehaviour
{

    [SerializeField]
    private Transform left, right;
    [SerializeField]
    private float maxDistanceDelta = 0.005f;
    [SerializeField]
    private float margin = 0.1f;
    [SerializeField]
    private float speedFlip = 3.0f;

    private bool goLeft = true;

    void Update()
    {

        if (GameManager.instance.gameOver) return;

        if (goLeft)
        {
            transform.position = Vector2.MoveTowards(transform.position, left.position, maxDistanceDelta);

            if (Vector2.Distance(transform.position, left.position) < margin)
            {
                goLeft = false;
                StartCoroutine(Flip(goLeft));
            }
        }
        else if (!goLeft)
        {
            transform.position = Vector2.MoveTowards(transform.position, right.position, maxDistanceDelta);

            if (Vector2.Distance(transform.position, right.position) < margin)
            {
                goLeft = true;
                StartCoroutine(Flip(goLeft));
            }
        }
    }

    IEnumerator Flip(bool _goLeft)
    {
        while (_goLeft)
        {
            transform.localScale = new Vector2(transform.localScale.x + speedFlip * Time.deltaTime, 1);
            if (transform.localScale.x >= 1)
            {
                transform.localScale = Vector3.one;
                break;
            }

            yield return null;
        }
        while (!_goLeft)
        {
            transform.localScale = new Vector2(transform.localScale.x - speedFlip * Time.deltaTime, 1);
            if (transform.localScale.x <= -1)
            {
                transform.localScale = new Vector3(-1, 1, 1);
                break;
            }

            yield return null;
        }
    }
}