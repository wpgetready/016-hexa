﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Script which activates the gravity of pieces
/// </summary>
namespace MadFireOn
{
    public class GravityActivator : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        //when the piece comes in contact with activate its gravity is set active
        void OnTriggerEnter2D(Collider2D other)
        {
			if (other != null && other.CompareTag("Pieces"))
            {
                //the activator collider only collides with piece so we dont have to specify any tags ,names, etc
#if UNITY_5_4//_OR_NEWER
                other.GetComponent<PieceScript>().isRayOn = true;
#else
	//			if (other.tag !="Player") {
					var myOther = other.GetComponent<PieceScript>();
					myOther.isKinematic =false;	
	//			}
#endif
            }
        }
    }
}//namespace
/*if (other.tag !="Player") {
					var myOther = other.GetComponent<PieceScript>();
					myOther.isKinematic =false;	
				}*/
