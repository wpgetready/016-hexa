﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
using UnityEngine.SceneManagement;

namespace MadFireOn
{
    public class LeaderboardiOSManager : MonoBehaviour
    {

        public static LeaderboardiOSManager instance;

        public string leaderBoardID = "PROVIDE_YOUR_LEADERBOARD_ID_HERE";

        // *************************************************************New Code
        //provide your achievement IDs here , depending on number of achievements you can add more or less
        private const string Master = "com.compnayname.demo.achievement1";
        private const string Pro = "com.companyname.demo.achievement2";
        private const string God = "com.companyname.demo.achievement3";
        private const string UnlockMaster = "com.companyname.demo.achievement4";
        private const string UltraMaster = "com.companyname.demo.achievement4";

        private string[] achievements_names = { Master, Pro, God, UnlockMaster, UltraMaster };

        private bool[] achievements; //ref variable to check is achievemnt is unlocked or not
                                     // *************************************************************New Code


        #region GAME_CENTER	

        /// <summary>
        /// Authenticates to game center.
        /// </summary>

        // *************************************************************New Code
        void OnEnable()
        {
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }
        // *************************************************************New Code


        void Awake()
        {
            MakeInstance();
        }

        void MakeInstance()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        void Start()
        {
            AuthenticateToGameCenter();
        }

        // *************************************************************New Code
        void InitializeAchievements()
        {   //we assign all the values stored in game manager to bool array
            achievements = GameManager.instance.achievements;
            //we call a for loop to check all achievement status
            //for (int i = 0; i < achievements.Length; i++)
            //{   //is achievement is not unlocked
            //    if (!achievements[i])
            //    {   //we call the achievement and set its progress to zero
            //        Social.ReportProgress(achievements_names[i], 0.0f, (bool success) =>
            //        {
            //        //handle success
            //    });
            //    }
            //}
        }
        // *************************************************************New Code


        public void AuthenticateToGameCenter()
        {
#if UNITY_IOS
            Social.localUser.Authenticate(success =>
                                          {
                                              if (success)
                                              {
                                                  Debug.Log("Authentication successful");
                                              // *************************************************************New Code
                                              Social.LoadAchievements(ProcessLoadedAchievements); //call to load the achievementts
                                              InitializeAchievements();
                                              // *************************************************************New Code
                                          }
                                              else
                                              {
                                                  Debug.Log("Authentication failed");
                                              }
                                          });
#endif
        }

        /// <summary>
        /// Reports the score on leaderboard.
        /// </summary>
        /// <param name="score">Score.</param>
        /// <param name="leaderboardID">Leaderboard I.</param>

        public static void ReportScore(long score, string leaderboardID)
        {

#if UNITY_EDITOR

            Debug.Log("Working");

#elif UNITY_IOS
        //Debug.Log("Reporting score " + score + " on leaderboard " + leaderboardID);
        Social.ReportScore(score, leaderboardID, success =>
		   {
			if (success)
			{
				Debug.Log("Reported score successfully");
			}
			else
			{
				Debug.Log("Failed to report score");
			}

			Debug.Log(success ? "Reported score successfully" : "Failed to report score"); Debug.Log("New Score:"+score);  
		});
#endif
        }

        // *************************************************************New Code
        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {
#if UNITY_IOS
            if (Social.localUser.authenticated == true)
            {
                ReportScore(MadFireOn.GameManager.instance.lastScore, leaderBoardID);
            }
#endif
        }
        // *************************************************************New Code

        /// <summary>
        /// Shows the leaderboard UI.
        /// </summary>

        public void ShowLeaderboard()
        {
#if UNITY_IOS
            Social.ShowLeaderboardUI();
#endif
        }

        // *************************************************************New Code
        // <summary>
        /// Shows the achievement UI.
        /// </summary>
        public void ShowAchievement()
        {
#if UNITY_IOS
            Social.ShowAchievementsUI();
#endif
        }
        //this is to check if we have any achievements in game center
        void ProcessLoadedAchievements(IAchievement[] achievements)
        {
            if (achievements.Length == 0)
                Debug.Log("Error: no achievements found");
            else
                Debug.Log("Got " +achievements.Length + " achievements");
        }

        //this method is called when scene is reloaded or changed to check for achievements . you can also call it in update but then it will be called every frame
        void CheckIfAnyUnlockedAchievements()
        {
            if (GameManager.instance != null)
            {
                if (GameManager.instance.currentScore >= 300)
                {
                    if (!achievements[0])
                        UnlockAchievements(0);
                }

                if (GameManager.instance.currentScore >= 600)
                {
                    if (!achievements[1])
                        UnlockAchievements(1);
                }

                if (GameManager.instance.currentScore >= 1200)
                {
                    if (!achievements[2])
                        UnlockAchievements(2);
                }

                if (GameManager.instance.currentScore >= 1600)
                {
                    if (!achievements[3])
                        UnlockAchievements(3);
                }
            }
        }

        void UnlockAchievements(int index)
        {
            if (Social.localUser.authenticated)
            {
                Social.ReportProgress(achievements_names[index], 100.0f, (bool success) =>
                {
                    if (success)
                    {
                        //sound.Play(); //here we play the soucd when we achieve new achievement
                        achievements[index] = true;
                        GameManager.instance.achievements = achievements;
                        GameManager.instance.Save();
                    }
                });
            }
        }

        // *************************************************************New Code

        #endregion
    }
}