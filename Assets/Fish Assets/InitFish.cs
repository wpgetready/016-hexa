﻿using UnityEngine;

public class InitFish : MonoBehaviour {

    [SerializeField]
    private GameObject particulas, renderizadorParticulas, effectCam;

    private bool firstFrame = true;


    void Awake()
    {
        Transform cam = Camera.main.transform;
        renderizadorParticulas.transform.SetParent(cam);
        effectCam.transform.SetParent(cam);
    }
    void LateUpdate()
    {
        if (firstFrame)
        {
            particulas.transform.SetParent(null);
            firstFrame = false; particulas.SetActive(true);
            Destroy(this);
        }
    }
}