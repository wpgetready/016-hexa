﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MadFireOn;


public class WaterManager : MonoBehaviour {

    [SerializeField]
    private int layerValue = 4;
    [SerializeField]
    private int minParticle = 6;
    [SerializeField]
    private int initParticle = 65;

    private int totalParticleStay = 0;


    void Start()
    {
        totalParticleStay = initParticle;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (GameManager.instance.gameOver) return;

        if (other.gameObject.layer == layerValue)
        {
            totalParticleStay++;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (GameManager.instance.gameOver) return;

        if (other.gameObject.layer == layerValue)
        {
            totalParticleStay--;

            if(totalParticleStay < minParticle)
            {
                GameManager.instance.gameOver = true;
            }
        }
    }
}
